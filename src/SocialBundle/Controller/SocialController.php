<?php
namespace SocialBundle\Controller;
use SocialBundle\Entity\commentaire;
use SocialBundle\Entity\user;
use SocialBundle\Entity\publication;
use SocialBundle\Entity\Message;
use SocialBundle\Entity\Groupe;
use SocialBundle\Form\GroupType;
use SocialBundle\Form\userType;
use SocialBundle\SocialBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Datetime;
use Symfony\Component\Validator\Constraints\File;

class SocialController extends Controller
{
    //action qui permettera d'inscrire les utilisateurs

    public function publierAction(Request $request)
    {
        $publication = new publication;
        $form = $this->createFormBuilder($publication)
        ->add('titre', TextType::class)
        ->add('contenu', TextareaType::class)
            ->add('image', FileType::class, [
                'label' => 'image',
                'required' => false,
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/png',
                            'application/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid png /jpeg  document',
                    ])
                ],
            ])
        ->add('publier', SubmitType::class)
        ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image= $form->get('image')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename.'-'.uniqid().'.'.$image->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('pimage_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $publication->setImage($newFilename);
            }
        $entityManager = $this->getDoctrine()->getManager();
        $repository=$entityManager->getRepository('SocialBundle:user');
        $user = $repository->findOneById($_SESSION["id"]);
        $publication->setUser($user);
        $publication->setDate(new Datetime());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($publication);
        $entityManager->flush();
        return $this->redirectToRoute('social_home');
        }
        return $this->render('@Social/Default/publier.html.twig',
        array('monFormulaire' => $form->createView()));
    }
    public function chercheruserAction(Request $request)
    {
        $user = new user;
        $form = $this->createFormBuilder($user)
        ->add('username', TextType::class)
        ->add('envoyer', SubmitType::class)
        ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=$entityManager->getRepository('SocialBundle:user');
        $users = $repository->finduserslike($user->getUsername());
            return $this->render('@Social/Default/resultats.html.twig',
                array('users' => $users));
        }
        return $this->render('@Social/Default/recherche.html.twig',
        array('monFormulaire' => $form->createView()));
    }
    public function feeddefaultAction()
    {
        $em = $this->getDoctrine()->getManager();
        $me=$_SESSION["id"];
        $requete = "(SELECT p.id as idp,p.titre ,p.image, p.contenu, p.image, p.date, u.id, u.username FROM user u, publication p, friends f WHERE u.id=p.user_id and ( f.friend_user_id=u.id and f.user_id=$me ) )UNION (SELECT DISTINCT p.id as idp,p.titre ,p.image, p.contenu, p.image, p.date, u.id, u.username FROM user u, publication p, friends f WHERE u.id=p.user_id and u.id=$me )";
        $resultat = $em->getConnection()->prepare($requete);
        $resultat->execute();

        return $this->render('@Social/Default/feed.html.twig',
            array('publications' => $resultat));
    }
    public function listeamisAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('SocialBundle:user');
        $me=$repository->find($_SESSION['id']);
        $amis=$me->getFriendsWithMe();
        return $this->render('@Social/Default/amis.html.twig',
            array('amis' => $amis));

    }
    public function ajouteramisAction($id)
    {
        $em = $this->getDoctrine()->getManager();
         $repository=$em->getRepository('SocialBundle:user');
        $me=$repository->find($_SESSION['id']);
        $myfriend=$repository->find($id);
        $me->addMyFriend($myfriend);
        $myfriend->addFriendswithme($me);
        $em->persist($me);
        $em->persist($myfriend);
        $em->flush();
        return $this->render('@Social/Default/profile.html.twig',
            array('user' => $myfriend,'possible'=>false,'itsme'=>false));


    }
    public function creergroupeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:user');
        $me = $repository->find($_SESSION['id']);
        $groupe = new Groupe;
        $form = $this->createForm(GroupType::class, $groupe);
        $form->add('envoyer', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $groupe->setcreateur($me);
            $groupe->addMembre($me);
            $em->persist($groupe);
            $em->flush();
           return $this->redirectToRoute('social_home');
        }
       return $this->render('@Social/Default/creergroup.html.twig', array('monFormulaire' => $form->createView()));
    }
     public function sendmessageAction($username,Request $request){
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('SocialBundle:user');
         $me = $repository->find($_SESSION['id']);
         $myreciever =$repository->findOneByUsername($username);
         $message =new Message;
         $form=$this->createFormBuilder($message)
             ->add('contenu', TextareaType::class)
             ->add('envoyer', SubmitType::class)
             ->getForm();
           $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setSender($me);
            $message->setReciever($myreciever);
            $message->setdate(new DateTime(date("m/d/y")));
            $em->persist($message);
            $em->flush();
            return $this->redirectToRoute('social_messagerie');
        }
        return $this->render('@Social/Default/contacter.html.twig', array('monFormulaire' => $form->createView()));

    }
    public function messagerieAction(){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:user');
        $me = $repository->find($_SESSION['id']);
        $repository = $em->getRepository('SocialBundle:Message');
        $mesmessages=$repository->findByReciever($me);
        return $this->render('@Social/Default/messagerie.html.twig', array('mesmessages' => $mesmessages));

    }
    public function mesgroupesAction(){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:user');
        $me = $repository->find($_SESSION['id']);
        $id =$me->getId();
        $requete="SELECT * from groupe g where g.id in(select groupe_id from appartientgroupe where user_id =$id)";
        $resultat = $em->getConnection()->prepare($requete);
        $resultat->execute();
        return $this->render('@Social/Default/mesgroupes.html.twig', array('groupes' => $resultat));

    }
    public function cherchergroupeAction(Request $request){
        $groupe = new Groupe;
        $form = $this->createFormBuilder($groupe)
                     ->add('nom', TextType::class)
                     ->add('chercher', SubmitType::class)
                     ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('SocialBundle:Groupe');
            $groupec= $repository->findByNom($groupe->getNom());
            return $this->render('@Social/Default/resultatsg.html.twig',
                array('groupes' => $groupec));
        }


        return $this->render('@Social/Default/cherchergroupe.html.twig', array('monFormulaire' => $form->createView()));

    }
    public function publicationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:publication');
        $publi= $repository->find($id);
        $commentaires=$publi->getcommentaires();
        return $this->render('@Social/Default/publication.html.twig', array('publication' => $publi));
        

    }
    public function groupehomeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:Groupe');
        $groupe= $repository->find($id);
        return $this->render('@Social/Default/groupe.html.twig', array('groupe' => $groupe));
    }
    public function groupefeedAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('SocialBundle:Groupe');
        $groupe= $repository->find($id);
        $publications = $groupe->getPublications();
        return $this->render('@Social/Default/feedgroup.html.twig',
            array('publications' => $publications));

    }
    public function publiergroupeAction(Request $request,$idg)
    {
        $publication = new publication;
        $form = $this->createFormBuilder($publication)
            ->add('titre', TextType::class)
            ->add('contenu', TextareaType::class)
            ->add('image', FileType::class, [
                'label' => 'image',
                'required' => false,
                'data_class' => null,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/png',
                            'application/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid png /jpeg  document',
                    ])
                ],
            ])
            ->add('publier', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $image= $form->get('image')->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename.'-'.uniqid().'.'.$image->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('pimage_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $publication->setImage($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $repository=$entityManager->getRepository('SocialBundle:user');
            $user = $repository->findOneById($_SESSION["id"]);
            $publication->setUser($user);
            $repository=$entityManager->getRepository('SocialBundle:Groupe');
            $groupe= $repository->find($idg);
            $publication->setGroupe($groupe);
            $publication->setDate(new Datetime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($publication);
            $entityManager->flush();
            return $this->redirectToRoute('social_groupe',array('id'=>$idg));
        }
        return $this->render('@Social/Default/publier.html.twig',
            array('monFormulaire' => $form->createView()));
    }
    public function commenterAction(Request $request,$id)
    {
        $commentaire=new commentaire();
         $form = $this->createFormBuilder($commentaire)
            ->add('contenu',TextareaType::class)
            ->add('envoyer',SubmitType::class)
            ->getForm();
         $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $repository=$entityManager->getRepository('SocialBundle:user');
            $user = $repository->find($_SESSION["id"]);
            $commentaire->setUser($user);
            $repository=$entityManager->getRepository('SocialBundle:publication');
            $publication=$repository->find($id);
            $commentaire->setPublication($publication);
            $commentaire->setDate(new DateTime(date("m/d/y")));
            $entityManager->persist($commentaire);
            $entityManager->flush();
            return $this->redirectToRoute('social_publication',array('id'=>$id));
        }
        return $this->render('@Social/Default/commenter.html.twig',
            array('monFormulaire' => $form->createView()));

    }



}
