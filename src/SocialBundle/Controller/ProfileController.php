<?php
namespace SocialBundle\Controller;
use SocialBundle\Entity\user;
use SocialBundle\Entity\publication;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SocialBundle\Form\userType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;


use \Datetime;

class ProfileController extends Controller
{
    public function accueilAction(Request $request)
    {
        $user = new user;
        $form = $this->createForm(userType::class, $user);
        $form->add('envoyer', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (){}
            else if (){}
            else {
                $entityManager = $this->getDoctrine()->getManager();
                $user->setMotdepasse(password_hash($user->getMotdepasse(), PASSWORD_DEFAULT));
                $user->setPhotoProfile('default_pp.png');
                $entityManager->persist($user);
                $entityManager->flush();
                $message="votre compte a été crée avec succés vous pouvez vous connecter dés a present";
                return $this->redirectToRoute('social_connexion',array('message'=>$message));
            }
        }
        return $this->render('@Social/Default/accueil.html.twig',
            array('monFormulaire' => $form->createView()));
    }

    public function homeAction()
    {
        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('SocialBundle:user');
        $user = $repository->findOneById($_SESSION["id"]);

        return $this->render('@Social/Default/home.html.twig',
            array('user' => $user));
    }
    //action qui permettera de connecter les utilisateurs
    public function connexionAction(Request $request,$message)
    {
        $user = new user;
        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class)
            ->add('motdepasse', PasswordType::class)
            ->add('se connecter', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $repository = $entityManager ->getRepository('SocialBundle:user');
            $user1 = $repository->findOneByusername($user->getUsername());
            if ($user1 != null)
            {
                if (password_verify($user->getMotdepasse(),$user1->getMotdepasse()))
                {

                    $_SESSION["id"]=$user1->getId();
                    return $this->redirectToRoute('social_home');

                }
            }

        }
        return $this->render('@Social/Default/connexion.html.twig',
            array('monFormulaire' => $form->createView()));
    }
    public function deconnexionAction(Request $request)
    {
        unset($_SESSION["id"]);
        return $this->redirectToRoute('social_accueil');
    }
    public function updateAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager ->getRepository('SocialBundle:user');
        $user = $repository->findOneById($_SESSION["id"]);
        $form = $this->createForm(userType::class, $user);
        $form->add('modifier', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pdp= $form->get('photoProfile')->getData();
            if ($pdp) {
                $originalFilename = pathinfo($pdp->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = $originalFilename.'-'.uniqid().'.'.$pdp->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $pdp->move(
                        $this->getParameter('pdp_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $user->setPhotoProfile($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $user->setMotdepasse(password_hash($user->getMotdepasse(), PASSWORD_DEFAULT));

            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('social_afficher2');
        }
        return $this->render('@Social/Default/update.html.twig',
            array('monFormulaire' => $form->createView()));

    }
    public function profileAction(Request $request,$id){
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager ->getRepository('SocialBundle:user');
        $user = $repository->findOneById($id);
        $me=$repository->find($_SESSION['id']);
        $mesamis=$me->getMyFriends();
        $possible=true;
        $itsme=false;
        if (($user ==$me))$itsme= true;

        else {
            for ($i=0; $i<count($mesamis);$i++)
            {
                if ($mesamis[$i] == $user) $possible = false;
            }
        }
        return $this->render('@Social/Default/profile.html.twig',
            array('user' => $user,'possible'=>$possible,'itsme'=>$itsme));


    }

}
