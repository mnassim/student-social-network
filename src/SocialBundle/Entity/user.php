<?php

namespace SocialBundle\Entity;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * user
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\userRepository")
 */
class user
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=40, unique=true)
     */
    private $username;
    /**
     * @var string
     *
     * @ORM\Column(name="motdepasse", type="string", length=255)
     */

    private $motdepasse;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, unique=true)
     */

    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_naissance", type="date")
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_profile", type="string", length=255, nullable=true)
     */
    private $photoProfile;

    /**
     * Many Users have Many Users.
     * @ORM\ManyToMany(targetEntity="SocialBundle\Entity\user", mappedBy="myFriends")
     */
    private $friendsWithMe;

    /**
     * Many Users have many Users.
     * @ORM\ManyToMany(targetEntity="SocialBundle\Entity\user", inversedBy="friendsWithMe")
     * @ORM\JoinTable(name="friends",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_user_id", referencedColumnName="id")}
     *      )
     */
    private $myFriends;

    /** @ORM\ManyToMany(targetEntity="SocialBundle\Entity\Groupe",mappedBy="membres")
     */
    private $mesgroupes;
    /** @ORM\OneToMany(targetEntity="SocialBundle\Entity\Groupe",mappedBy="createur")
     */
    private $mesgroupespersonnels;

    /**
     * Get id
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return user
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return user
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return user
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return user
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return user
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return user
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set photoProfile
     *
     * @param string $photoProfile
     *
     * @return user
     */
    public function setPhotoProfile($photoProfile)
    {
        $this->photoProfile = $photoProfile;

        return $this;
    }

    /**
     * Get photoProfile
     *
     * @return string
     */
    public function getPhotoProfile()
    {
        return $this->photoProfile;
    }

    /**
     * Set motdepasse
     *
     * @param string $motdepasse
     *
     * @return user
     */
    public function setMotdepasse($motdepasse)
    {
        $this->motdepasse = $motdepasse;

        return $this;
    }

    /**
     * Get motdepasse
     *
     * @return string
     */
    public function getMotdepasse()
    {
        return $this->motdepasse;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->friendsWithMe = new \Doctrine\Common\Collections\ArrayCollection();
        $this->myFriends = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add friendsWithMe
     *
     * @param \SocialBundle\Entity\user $friendsWithMe
     *
     * @return user
     */
    public function addFriendsWithMe(\SocialBundle\Entity\user $friendsWithMe)
    {
        $this->friendsWithMe[] = $friendsWithMe;

        return $this;
    }

    /**
     * Remove friendsWithMe
     *
     * @param \SocialBundle\Entity\user $friendsWithMe
     */
    public function removeFriendsWithMe(\SocialBundle\Entity\user $friendsWithMe)
    {
        $this->friendsWithMe->removeElement($friendsWithMe);
    }

    /**
     * Get friendsWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriendsWithMe()
    {
        return $this->friendsWithMe;
    }

    /**
     * Add myFriend
     *
     * @param \SocialBundle\Entity\user $myFriend
     *
     * @return user
     */
    public function addMyFriend(\SocialBundle\Entity\user $myFriend)
    {
        $this->myFriends[] = $myFriend;

        return $this;
    }

    /**
     * Remove myFriend
     *
     * @param \SocialBundle\Entity\user $myFriend
     */
    public function removeMyFriend(\SocialBundle\Entity\user $myFriend)
    {
        $this->myFriends->removeElement($myFriend);
    }

    /**
     * Get myFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyFriends()
    {
        return $this->myFriends;
    }

    /**
     * Add mesgroupe
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupe
     *
     * @return user
     */
    public function addMesgroupe(\SocialBundle\Entity\Groupe $mesgroupe)
    {
        $this->mesgroupes[] = $mesgroupe;

        return $this;
    }

    /**
     * Remove mesgroupe
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupe
     */
    public function removeMesgroupe(\SocialBundle\Entity\Groupe $mesgroupe)
    {
        $this->mesgroupes->removeElement($mesgroupe);
    }

    /**
     * Get mesgroupes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMesgroupes()
    {
        return $this->mesgroupes;
    }

    /**
     * Set mesgroupespersonels
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupespersonels
     *
     * @return user
     */
    public function setMesgroupespersonels(\SocialBundle\Entity\Groupe $mesgroupespersonels = null)
    {
        $this->mesgroupespersonels = $mesgroupespersonels;

        return $this;
    }

    /**
     * Get mesgroupespersonels
     *
     * @return \SocialBundle\Entity\Groupe
     */
    public function getMesgroupespersonels()
    {
        return $this->mesgroupespersonels;
    }

    /**
     * Add mesgroupespersonel
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupespersonel
     *
     * @return user
     */
    public function addMesgroupespersonel(\SocialBundle\Entity\Groupe $mesgroupespersonel)
    {
        $this->mesgroupespersonels[] = $mesgroupespersonel;

        return $this;
    }

    /**
     * Remove mesgroupespersonel
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupespersonel
     */
    public function removeMesgroupespersonel(\SocialBundle\Entity\Groupe $mesgroupespersonel)
    {
        $this->mesgroupespersonels->removeElement($mesgroupespersonel);
    }

    /**
     * Add mesgroupespersonnel
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupespersonnel
     *
     * @return user
     */
    public function addMesgroupespersonnel(\SocialBundle\Entity\Groupe $mesgroupespersonnel)
    {
        $this->mesgroupespersonnels[] = $mesgroupespersonnel;

        return $this;
    }

    /**
     * Remove mesgroupespersonnel
     *
     * @param \SocialBundle\Entity\Groupe $mesgroupespersonnel
     */
    public function removeMesgroupespersonnel(\SocialBundle\Entity\Groupe $mesgroupespersonnel)
    {
        $this->mesgroupespersonnels->removeElement($mesgroupespersonnel);
    }

    /**
     * Get mesgroupespersonnels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMesgroupespersonnels()
    {
        return $this->mesgroupespersonnels;
    }
    /**
     * @Assert\IsTrue(message="votre mot de passe est trop court")
     */
    public function ispasswordtooshort()
    {
        $mdp=$this->motdepasse;

        if(strlen($mdp)>8 )
                            return true;
        else return false;
    }
    /**
     * @Assert\IsTrue(message="il faut au moins une majuscule dans votre mot de passe")
     */
    public function ispasswordcontainingmaj()
    {
        $mdp=$this->motdepasse;

        if(preg_match('#[A-Z]#', $mdp) )
            return true;
        else return false;
    }
    /**
     * @Assert\IsTrue(message="il faut au moins un chiffre dans votre mot de passe")
     */
    public function ispasswordcontainingnumbers()
    {
        $mdp=$this->motdepasse;

        if(preg_match('#[0-9]#', $mdp) )
            return true;
        else return false;
    }
}
