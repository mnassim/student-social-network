<?php

namespace SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groupe
 *
 * @ORM\Table(name="groupe")
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\GroupeRepository")
 */
class Groupe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /** @ORM\ManyToMany(targetEntity="SocialBundle\Entity\user",inversedBy="mesgroupes")
     * @ORM\JoinTable(name="appartientgroupe")
     */
    private $membres;
    /** @ORM\ManyToOne(targetEntity="SocialBundle\Entity\user", inversedBy="mesgroupespersonnels")
     */
    private $createur;
    /** @ORM\OneToMany(targetEntity="SocialBundle\Entity\publication", mappedBy="groupe") */
    private $publications;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Groupe
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Groupe
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->membres = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add membre
     *
     * @param \SocialBundle\Entity\user $membre
     *
     * @return Groupe
     */
    public function addMembre(\SocialBundle\Entity\user $membre)
    {
        $this->membres[] = $membre;

        return $this;
    }

    /**
     * Remove membre
     *
     * @param \SocialBundle\Entity\user $membre
     */
    public function removeMembre(\SocialBundle\Entity\user $membre)
    {
        $this->membres->removeElement($membre);
    }

    /**
     * Get membres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembres()
    {
        return $this->membres;
    }

    /**
     * Set createur
     *
     * @param \SocialBundle\Entity\user $createur
     *
     * @return Groupe
     */
    public function setCreateur(\SocialBundle\Entity\user $createur = null)
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return \SocialBundle\Entity\user
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * Add createur
     *
     * @param \SocialBundle\Entity\user $createur
     *
     * @return Groupe
     */
    public function addCreateur(\SocialBundle\Entity\user $createur)
    {
        $this->createur[] = $createur;

        return $this;
    }

    /**
     * Remove createur
     *
     * @param \SocialBundle\Entity\user $createur
     */
    public function removeCreateur(\SocialBundle\Entity\user $createur)
    {
        $this->createur->removeElement($createur);
    }
    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * Add publication
     *
     * @param \SocialBundle\Entity\publication $publication
     *
     * @return Groupe
     */
    public function addPublication(\SocialBundle\Entity\publication $publication)
    {
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \SocialBundle\Entity\publication $publication
     */
    public function removePublication(\SocialBundle\Entity\publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }
}
