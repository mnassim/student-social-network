<?php

namespace SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="SocialBundle\Repository\commentaireRepository")
 */
class commentaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    /** @ORM\ManyToOne(targetEntity="SocialBundle\Entity\user") */
    private $user;
    /** @ORM\ManyToOne(targetEntity="SocialBundle\Entity\publication", inversedBy="commentaires") */
    private $publication;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return commentaire
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return commentaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \SocialBundle\Entity\user $user
     *
     * @return commentaire
     */
    public function setUser(\SocialBundle\Entity\user $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SocialBundle\Entity\user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set publcation
     *
     * @param \SocialBundle\Entity\publication $publcation
     *
     * @return commentaire
     */
    public function setPublcation(\SocialBundle\Entity\publication $publcation = null)
    {
        $this->publcation = $publcation;

        return $this;
    }

    /**
     * Get publcation
     *
     * @return \SocialBundle\Entity\publication
     */
    public function getPublcation()
    {
        return $this->publcation;
    }

    /**
     * Set publication
     *
     * @param \SocialBundle\Entity\publication $publication
     *
     * @return commentaire
     */
    public function setPublication(\SocialBundle\Entity\publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \SocialBundle\Entity\publication
     */
    public function getPublication()
    {
        return $this->publication;
    }
}
