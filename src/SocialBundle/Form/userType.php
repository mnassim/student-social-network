<?php
namespace SocialBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class userType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('username', TextType::class)
            ->add('email', EmailType::class)
            ->add('motdepasse', PasswordType::class)
            ->add('date_naissance', DateType::class,[  'years' => range(1900,2005) ])
            ->add('photoProfile', FileType::class, [
            'label' => 'photo de profile',
            'required' => false,
                'data_class' => null,
            'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/png',
                        'application/jpeg',
                    ],
                    'mimeTypesMessage' => 'Please upload a valid png /jpeg  document',
                ])
            ],
        ]);
    }
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SocialBundle\Entity\user',
        ));

    }
}